DEST ?= prj
USERNAME ?= 192.168.23.206
TARGET = mbdump
CC = arm-linux-gnueabihf-gcc
CFLAGS = 
LDFLAGS = 

INCLUDE = -I./WiringPi/wiringPi/
LIBS = -lwiringPi
LIBDIR = -L./usr/

SRC = $(wildcard src/*.c)

OBJ = $(patsubst %.c, %.o, $(SRC))

all: $(TARGET)

$(TARGET): $(OBJ)
	$(CC) -o ./Applications/$@ $^ $(LDFLAGS) $(LIBDIR) $(LIBS)

%.o: %.c
	$(CC) -c -o $@ $< $(CFLAGS) $(INCLUDE)

.PHONY: clean
clean:
	rm -f src/*.o $(TARGET)

install: $(TARGET)
	         scp ./Applications/$(TARGET) pi@$(USERNAME):/home/pi/$(DEST)
	         scp ./usr/libwiringPi.so pi@$(USERNAME):/home/pi/$(DEST)





