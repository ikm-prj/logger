//****************************************************************************
//*                                                                          *
//*                INDUSTRIJSKE KOMUNIKACIONE MREŽE 2322                     *
//*                                                                          *
//****************************************************************************
//*                                                                          *
//* Filename: mbdump.c		                                                 *
//* Date: 13/07/2020                                                         *
//* File Version: 1.1                                                        *
//*                                                                          *
//* Authors: -Vanja Žerić 1201/17                                            *  
//* 	     -Nemanja Koprena 1244/17                                        *
//*          -Nemanja Ćibić 1220/15                                          *
//*                                                                          *
//* Mentor:   Mladen Knežić                                                  *
//*                                                                          *
//* Company: Elektrotehnički fakultet Banja Luka                             *
//*                                                                          *
//****************************************************************************
//*                                                                          *
//* Files Required: Application for sending modbus frame                     *
//*                                                                          *
//****************************************************************************
//*                                                                          *
//* Description: Application mbdump catches frames from bus                  *
//*                                                                          *
//***************************************************************************/



///*** BIBLIOTEKE ***///

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wiringPi.h>
#include <string.h>
#include <termios.h>
#include <fcntl.h>

///*** KONSTANTE ***///

#define SERVER_ID		1

#define COIL_ADDRESS0		0
#define COIL_ADDRESS1		1
#define COIL_ADDRESS2		2
#define COIL_ADDRESS3		3

#define BCM_PIN_DE		22

#define Function_Code_1 			"Read Coil"
#define Function_Code_2 			"Read Discrete Input"
#define Function_Code_3 			"Read Holding Registers"
#define Function_Code_4 			"Read Input Registers"
#define Function_Code_5 			"Write Single Coil"
#define Function_Code_6 			"Write Single Holding Register"
#define Function_Code_15 			"Write Multiple Coils"
#define Function_Code_16 			"Write Multiple Holding Registers"

#define Function_Code_ERR1 			"Illegal Function"
#define Function_Code_ERR2 			"Illegal Data Address"
#define Function_Code_ERR3 			"Illegal Data Value"
#define Function_Code_ERR4 			"Slave Device Failure"
#define Function_Code_ERR6 			"Slave Device Busy"
#define Function_Code_ERR10 		"Gateway Path Unavailable"
#define Function_Code_ERR11 		"Gateway Target Device Failed to Respond"

#define Function_Code_Non			"User Defined Function Code"

#define FILTER                      2 //Ono sto ne zelimo da ispisuje
//Ukoliko ne zelimo filtriranje potrebno je zakomentarisati filter i if petlju na liniji 200 ili postaviti na neku vrijednost adrese koja ne postoji, Linija označena sa //***  

///*** PROTOTIP FUNKCIJA ***///

char* Convert_Function_Code_To_Message(char Function_Code_Ocitan);


///*** MAIN ***///

int main( int argc, char *argv[] )  {
    
    //Provjera ispravnosti prosljedjenih parametara kroz terminal
    if( argc == 2 ) {
        printf("The argument supplied is %s\n", argv[1]); //Treba biti /dev/ttyAMA0 kad se pokrene program sa mbdump /dev/ttyAMA0!
    }
    else if( argc > 2 ) {
        printf("Too many arguments supplied!\n");
    }
    else {
        printf("One argument expected!\n");
    }
    
    //Potrebne promjenljive
    time_t seconds,seconds1,seconds2;
    char Function_Code_Ocitan;//Function code zauzima 1B odnosno 8bita!
    char Function_Code_Prosli; //Za odluku response/request
    char *Function_Code_ispis=(char*)calloc(45, sizeof(char)); //Function_Code_ispis sadzi poruku, a najduza je 39 slova
    int fd;
    int nbytes=0;
    int kontrolni=0;
    int brojac=0; //broji koji put ocitava function code, jer treba svaki drugi put da poredi Function_Code_Prosli sa Function_Code_Ocitan da vidi da li je u povratu, tj da li hvata response
    char buffer[257]={0};
    char node_address;
    char data[253]={0};
    char crc[2]={0};
    char frame_type[9];
    int Modbus_address;
    struct termios config;
    if (wiringPiSetup () == -1)
    {
        fprintf (stdout, "Unable to start wiringPi.\n") ;
        return -1;
    }
    fd = open(argv[1], O_RDWR | O_NOCTTY);   
    if (fd < 0)
        fprintf(stderr, "Failed to open serial port. Check if it is used by another device.\n");
    if (tcgetattr(fd, &config) < 0)
    {
        fprintf(stdout, "Unable to get configuration.\n");
        return -1;
    }
    cfmakeraw(&config);
    cfsetispeed(&config, B9600);
    cfsetospeed(&config, B9600);
    config.c_cc[VMIN] = 1;
    config.c_cc[VTIME] = 0;
  //  config.c_cflag |= PARENB;
    pinMode(3, OUTPUT);
	  
	// set it initially to be in receive mode
	digitalWrite(3, LOW);
    if (tcsetattr(fd, TCSANOW, &config) < 0)
    {
        fprintf(stdout, "Unable to set the configuration.\n");
        return -1;
    }
    
    //Referentna vrijednost za TimeStamp!
    seconds1 = time(NULL); //Uzima vrijeme od 1.1.1970. u sekundama
    
    //Formatirani ispis
    printf("==============================================================================================================================================================================\n");
    printf("|TimeStamp| Node Address  | Frame Type | Function Type | Modbus Address | [N-Bytes] |      Type      |  Hexadecimal Notation  |         Description of Function Code         |\n");
    printf("==============================================================================================================================================================================\n");
    //npr   |   5     | Adresa iz ocit 1B| v | 00000001      | iz data citamo 2B|[ocitano sa read]| int         | 45 AD 54 32 23 00 00 00|   Read Coil 
    
    while(1)
    {
        
        int number_of_bytes=read(fd,buffer,256);
        nbytes=number_of_bytes-4;
        if(number_of_bytes>=4){
            int i=2;
            node_address=buffer[0];
            Function_Code_Ocitan=buffer[1];
            if (brojac==0)
            {
                //u prvom koraku uhvati, a u drugom koraku (kad je brojac 1) ce provjeravati da li je isti kao novi frejm
                Function_Code_Prosli=buffer[1];//Za poredjenje za response/request
            }
            for(;i<number_of_bytes-2;i++)
                data[i-2]=buffer[i];
            crc[0]=buffer[i++];
            crc[1]=buffer[i];
            
            if(node_address==0){
                strcpy(frame_type,"Request");
                brojac=2;
            }
            else if ( (brojac == 1 && Function_Code_Ocitan==Function_Code_Prosli) || (Function_Code_Ocitan>127) ) //ako je brojac 1(jednom je prosao uhvaceni f.c.) i ako je novi ocitani i uhvaceni jednaki ili ako vraca error code onda je response sigurno!
            {
                strcpy(frame_type,"Response");
                kontrolni=1; // ako je kontrloni = 1 vracamo brojac na 0 na kraju while petlje, poslije brojac++ jer ako ga sad vratim dole ce se uvecati ponovo na 1 i nista nismo napravili
            }
            else
            {
                strcpy(frame_type,"Request"); // ako nije ni node adress nije 0 niti je ovo iznad ispunjeno mora biti request!
            }
            
            if(data)
            {	
                Modbus_address=data[0]*256;
                Modbus_address+=data[1];
            }
            else
            {
                Modbus_address=-1; //Nema podataka pa ne ocitava Modbus adresu
                
            }
            
            //Poziv funkcije za konverziju funkcijskog kod u znacenje
            Function_Code_ispis=Convert_Function_Code_To_Message(Function_Code_Ocitan);
            
            //TimeStamp
            seconds2 = time(NULL); //opet ce uzeti vrijeme od 1.1.1970 u sekundama 
            seconds=seconds2-seconds1;//oduzimanjem ova dva vremena dobijamo vrijeme u sekundama od otvaranja programa a kako se sve vrti u petlji i imamo blokirajucu funkciju prilikom ocitavanja ovo ce se ispisivati samo u trenucima ocitavanja, pa cemo znati nakon koliko sekundi se desilo ocitavanje!
            
            //Filtriranje => Ako je node_address = FILTER nece ispisati, tj. filtrirace ga!
            if(node_address != FILTER) //*** - Pazite na zagrade od if petje oznacicemo ih takodje sa //***
            { //***
                
            //Ispis
            printf("|%9ld|%15x|%12s|%15d|%16x| [%7d] |%16s|",seconds,node_address,frame_type, Function_Code_Ocitan,Modbus_address,nbytes-2,"int");
            
            for(i=2;i<nbytes;i++)
                printf("%02x ",data[i]);
            for(i=26-2*nbytes;i>0;i--)
                printf(" ");
            
            printf("|%46s|\n",Function_Code_ispis);
            printf("==============================================================================================================================================================================\n");
            for(i=0;i<252;i++)
                data[i]='\0';
            for(i=0;i<256;i++)
                buffer[i]='\0';
            brojac+=1;
            if(kontrolni==1 || brojac > 1)
            {
                brojac=0;
                kontrolni=0;
            }
            } //***
        }
        else
        {
             printf("Frame size error! Minimal needed Node Address (1B), Function Code (1B) and CRC (2B)\n");
             printf("==============================================================================================================================================================================\n");
            
        }
        
    }
    return 0;
}






///*** IMPLEMENTACIJE FUNKCIJA ***///

char* Convert_Function_Code_To_Message(char Function_Code_Ocitan)
{
    char* Function_Code_ispis=(char*)calloc(45, sizeof(char));
    //prvo provjera funkcijskog koda
    if (Function_Code_Ocitan==1){
        strcpy(Function_Code_ispis,Function_Code_1);
    }
    else if (Function_Code_Ocitan==2){
        strcpy(Function_Code_ispis,Function_Code_2);
    }
    else if (Function_Code_Ocitan==3){
        strcpy(Function_Code_ispis,Function_Code_3);
    }
    else if (Function_Code_Ocitan==4){
        strcpy(Function_Code_ispis,Function_Code_4);
    }
    else if (Function_Code_Ocitan==5){
        strcpy(Function_Code_ispis,Function_Code_5);
    }
    else if (Function_Code_Ocitan==6){
        strcpy(Function_Code_ispis,Function_Code_6);
    }
    else if (Function_Code_Ocitan==15){  // 15 ASCI
        strcpy(Function_Code_ispis,Function_Code_15);
    }
    else if (Function_Code_Ocitan==16){
        strcpy(Function_Code_ispis,Function_Code_16);
    }
    //sada error provjeravamo
    else if (Function_Code_Ocitan==129){
        strcpy(Function_Code_ispis,Function_Code_ERR1);
    }
    else if (Function_Code_Ocitan==130){
        strcpy(Function_Code_ispis,Function_Code_ERR2);
    }
    else if (Function_Code_Ocitan==131 ){
        strcpy(Function_Code_ispis,Function_Code_ERR3);
    }
    else if (Function_Code_Ocitan==132 ){
        strcpy(Function_Code_ispis,Function_Code_ERR4);
    }
    else if (Function_Code_Ocitan==134){
        strcpy(Function_Code_ispis,Function_Code_ERR6);
    }
    else if (Function_Code_Ocitan==138){
        strcpy(Function_Code_ispis,Function_Code_ERR10);
    }
    else if (Function_Code_Ocitan==139){
        strcpy(Function_Code_ispis,Function_Code_ERR11);
    }
    //ako nije neki od poznatih onda dajemo obavijestenje
    else{
        strcpy(Function_Code_ispis,Function_Code_Non);
    }
    
    //Function_Code_ispis je ono sto cemo ispisivati umjesto funkcijskog koda da korisnik ne mora da trazi sta znaci funkcijski kod!
    
    return Function_Code_ispis;
}









