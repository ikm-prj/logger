**Izvještaj za projektni zadatak iz predmeta Industrijske komunikacione mreže**

Zadatak: Projektovati **mbdump** - aplikaciju po uzoru na **candump** koja će se koristiti za modbus.
Uputstvo za korištenje aplikacije

---

## PRVI KORAK

Prvo je potrebno preuzeti projekat sa gita:

```
git clone https://vzeric98@bitbucket.org/ikm-prj/logger.git
```
Nakon preuzimanja repozitorijuma, potrebno je preuzeti potrebne biblioteke za bildanje fajlova, jer ove biblioteke se neće preuzeti prilikom kloniranja repozitorijuma, ali će folderi biti vidljivi, pa je potrebno voditi računa. 
Najprije je potrebno ući u repozitorijum:
```
cd /path/to/logger/
```
Za početak ćemo preuzeti libmodbus repozitorijum:
```
cd ./Required/
git clone --depth=1 https://github.com/dhruvvyas90/libmodbus
```
Ukoliko ne postoje komande **automake** **autoconf** **libtool** potrebno je pokrenuti sledeće, a ako postoje ovaj korak preskočiti
```
sudo apt-get install -y automake autoconf libtool
```
Ukoliko ne postoji nekim slučajem repozitorijum **usr** na lokaciji /path/to/logger/Required onda ga je potrebno napraviti:
```
mkdir usr
```
Ukoliko postoji, onda nije potrebno praviti direktorijum

Takođe, postoji mogućnost da unutar **usr** direktorijuma postoje dinamičke biblioteke za bildanje aplikacije, ali svakako savijetujemo da se odrade i naredni korace, iz sigurnosnih razloga, bitno je da se ispoštuju nazivi svih direktorijuma, jer će se kompajliranje vršiti kroz pripremljene **Makefile**-ove. Da bismo u potpunosti završili postupak potrebno je ispratiti naredne korake:
```
cd libmodbus
```
Nakon toga:
```
./autogen.sh
```
Zatim:
```
./configure ac_cv_func_malloc_0_nonnull=yes --prefix=/path/to/logger/Required/usr --host=arm-linux-gnueabihf
```
Primjer moje lokacije bi bio: **/home/vanjazeric/Desktop/IKM-Projektni2020/logger/Required/usr**
Putanja se lako moze provjeriti korištenjem komande **pwd**

Nakon svega:
```
make
```
Pa odmah zatim:
```
make install
```

Nakon toga ce postojati folder **libmodbus** sa potrebnim bibliotekama, a u **/Required/usr/** će postojati heder fajlovi i svi drugi potrebni za kompajliranje!
**libmodbus** heder je bitan za pripremljenu aplikaciju koja šalje modbus okvire!

Kada smo završili sa **libmodbus** potrebno je dobiti **WiringPi** jer **mbdump** direknto koristi ovu biblioteku, zbog povezivanja samih **RaspberryPi** i **Modbus** čvorova uz pomoć **UART**-a (/dev/ttyAMA0) i transivera **RS-485** koji se već nalazi na samoj pločici koja povezuje **Modbus** čvor sa RPi.

Potrebno je uraditi sledeće:
Kako se nalazimo na lokaciji .../Required/ potrebno se prvo vratiti na lokaciju /logger/

```
cd ..    //Korak nazad ili ako ste već napustili lokaciju bitno je da se nalazite na /path/to/logger
```
Zatim potrebno je preuzeti repozitorijum, a preuzimamo ga komandom:
```
git clone --depth 1 https://github.com/WiringPi/WiringPi
```
Zatim se pokreće:
```
make CC=arm-linux-gnueabihf-gcc
```
Nakon toga smo dobili WiringPi direktorijum. Nakon toga potrebno je pronaći **libwiringPI.so.2.60** (obično na lokaciji **/logger/WiringPi/wiringPi/**) zatim je preimenovati u **libwiringPi.so** (obrisati broj verzije u ovom slučaju 2.60) i prekopirati na lokaciju **/logger/usr**. Takođe, izuzetno je važno da se ispoštuju imena direktorijuma, se i kompajliranje aplikacije mbdump vrši preko pripremljenog **Makefile**.

---

## KOMPAJLIRANJE

Svaka aplikacija koja se kompajlira šalje se na lokaciju **/path/to/logger/Applications**, a svi izvorni fajlovi se nalaze u **/path/to/logger/src**, odnosno **/path/to/logger/Required/src**, ali o tome nije potrebno voditi računa jer je sve pripremljeno kroz **Makefile**.

Da bismo kompajlirali aplikaciju koja šalje modbus okvire potrebno je uraditi sledeće:
```
cd /path/to/logger/Required/
make
```
Ovo će napraviti aplikaciju **mbsender** smjestiti je u direktorijum **Applications**.
Pokretanjem
```
make install 
```
ova aplikacija će se kopirati na RPi, i to na RPi sa adresom **192.168.23.212** koji smo mi koristili za testiranje, i na adresu **/home/pi/prj**. Takođe, parametri **DEST** i **USERNAME** su promjenljivi pa je moguće pokretanjem kroz terminal promijeniti IP adresu i destinaciju:
```
make install USERNAME="**željena_adresa**" DEST="**željena_destinacija**"
```
Ili jednostavno mjenjanjem željenih parametara unutar **Makefile**-a.

Da bismo kompajlirali aplikaciju za hvatanje ovira **mbdump** potrebno je pokrenuti sledeće:
```
cd /path/to/logger/
make
```
Ovo će aplikaciju **mbdump** smjestiti u **Applications**.
Pokretanjem
```
make install 
```
ova aplikacija će se kopirati na RPi, i to na RPi sa adresom **192.168.23.206** koji smo mi koristili za testiranje, i na adresu **/home/pi/prj**. Takođe, parametri **DEST** i **USERNAME** su promjenljivi pa je moguće pokretanjem kroz terminal promijeniti IP adresu i destinaciju:
```
make install USERNAME="**željena_adresa**" DEST="**željena_destinacija**"
```
Ili jednostavno mjenjanjem željenih parametara unutar **Makefile**-a
Takođe, prije svakog kompajliranja, u izvornim kodu na lokaciji **/logger/src/mbdupm.c** može se promjeniti i filter za filtriranje po adresi čvora. Po defaultu je postavljen na 2, tj. da filtrira sve adrese čvora koje su jednake 2. Filtar je moguće i isključiti, ukoliko se zakomentarišu mjesta koja su označena u izvornom kodu, ili postavljanjem na neku adresu koja ne postoji.

---

## POVEZIVANJE
Potrebno je povezati **RaspberriPi** na modbus čvor sa relejima, koji će slati modbus okvire, i tako uključivati i isključivati releje, a na istu tačku je potrebno povezati i drugi **RaspberriPi** koji će da prikuplja frejmove i ispisuje ih isparsirane u vidu tabele. Paziti na **L** i **H**. **RaspberriPi** su povezani preko pločice koja se direktno povezuje na **RaspberriPi**, kačeći se direkno na **GPIO** pinove, koja sadrži transiver **RS-485**. Paziti na **+** i **-**. Dodatno je moguće povezati i osciloskop. **GND** povezati na masu, a jednu sondu prikopčai na **L** drugu na **H**, da bismo vidjeli oba signala jer se radi o **diferencijalnom** prenosu.

---

## POKRETANJE APLIKACIJA

Ukoliko aplikacije nisu prepoznate kao izvršni moduli potrebno je uraditi:
```
chmod +x mbsender
```
odnosno
```
chmod +x mbdump
```
Ukoliko nisu već ranije eksportovane dinamičke biblioteke potrebno ih je eksportovati:
```
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/pi/prj
```
**/home/pi/prj** je lokacija gdje se nalazi dinamicka biblioteka, ukoliko je lokacija druga potrebno ju je promijeniti! Ovo je potrebno uraditi na oba **RaspberryPi** jer i **mbdump** i **mbsender** koriste dinamicke biblioteke, koje se na **RaspberryPi** prebacuju odmah pokretanjem gore navedene naredbe **make install**.

Prvo se pokrene **mbdump** koja je blokirajuća funkcija. Ova aplikacija pored naziva traži i argument komandne linije, zbog **UART**-a, ali prije pokretanja brzine potrebno je postaviti brzinu na 9600;
```
stty -F /dev/ttyAMA0 9600
```
Parnost, takođe, ne bi trebala biti uključena, jer je po defaultu isključena u **mbdump**-u:
```
stty -F /dev/ttyAMA0 -parenb
```
Zatim slijedi pokretanje aplikacije, a ona će ublokirati, jer koristi blokirajuću funkciju **read**, a kada nešto pročita, to će i da ispiše:
```
./mbdump /dev/ttyAMA0
```
Nakon pokretanja aplikacije mora biti izbačena prvo poruka da je **/dev/ttyAMA0/** odabran, a zatim se ispiše zaglavlje tabele, nakon čega funkcija ublokira čekajući očitavanje.

Nakon pokretanja **mbdump** prelazimo na drugi RPi i potrebno je pokrenuti aplikaciju **mbsender** i to ju je potrebno pokrenuti sa administratorskim privelgijama zbog **GPIO** pinova:
```
sudo ./mbsender
```
Ova funkcija će da po defaultu uključi relej, a nakon 5s da ga isključi. Očitavanje se vidi u tabeli!

---

